<?php get_header(); ?>

        </div>

        <div class="main-container">
            <div class="main wrapper clearfix">



    <article class="articles">
    <?php
            if(have_posts())
            {
                while(have_posts())
                {
                    the_post();?>

                    <h2> <?php the_title(); ?></h2>

                   <p id="date"> <?php the_date();?></p>
                   <?php
                    the_content();
                    ?><h3 class="comment-section">Comment section</h3><?php
                    comments_template();
                }
            }
            else
            {
                echo 'No content available';
            }
            ?>

    </article>

               <?php get_sidebar(); ?>
            </div> <!-- #main -->
        </div> <!-- #main-container -->


<?php get_footer(); ?>