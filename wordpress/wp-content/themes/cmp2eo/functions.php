
<?php
function register_menu()
{
    register_nav_menu('main-nav',__( 'Top Main Navigation' ));}
add_action( 'init', 'register_menu' );


?>

<?php
function my_wp_nav_menu_args( $args = '' ) {
if( is_user_logged_in() ) {
$args['menu'] = 'logged-in';
} else {
$args['menu'] = 'logged-out';
}
return $args;
}
add_filter( 'wp_nav_menu_args', 'my_wp_nav_menu_args' );
?>

<?php
if ( function_exists('register_sidebar') )
    register_sidebar();
?>


