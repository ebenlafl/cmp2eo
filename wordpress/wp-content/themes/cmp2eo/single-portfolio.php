<?php echo get_post_meta(get_the_ID(), 'NAAM_CUSTOM_FIELD', true); ?>

<?php
if(have_posts())
{
    while(have_posts())
    {
        the_post();
        //Print the title and the content of the current post
        the_title();
        the_content();
    }
}
else
{
    echo 'No content available';
}

$image = get_field('afbeelding');
$link = get_field('link', $image['ID']);
?>
<a href="<?php echo $link; ?>">
    <img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
</a>
?>
