<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cmp2');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'bitnami');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{:PR$sX%%VaSi)Ym>O8sr=|?y0[(oKi:cf+O4dqXm!F-f[ODJ|q|ho MIV&+Q mP');
define('SECURE_AUTH_KEY',  'S!S|V6M.@jYfe-Wz55&PtNOFZR|ENWYroP* .OI[US-q:]5}8TBG>>rIcvZWjr@=');
define('LOGGED_IN_KEY',    '(Mq9kWdQ5oFo*psK<NG*txSWSo0L-I=LB||<V%a=Rv&W>@wkeF3?F%tnBg-XtC|B');
define('NONCE_KEY',        '?tbeFf)0U!|yETvTNFbWD}f;W~{2{AztX=lJ)$ozt@WH74&g:?(]$@Q{5aM|5TRV');
define('AUTH_SALT',        'L-yI*Sb`)}!/660hf@yBT_e`x)[eEUiZ+jHh>#*Hz-_ggcP-4yU+-2{J7:!9DR2`');
define('SECURE_AUTH_SALT', 'Do3b6;%+~[#3uX=.<%3|b GZ<J$JULfaGacE|q!5+ISI-i;}R&o@Ww8tt?,{0zt+');
define('LOGGED_IN_SALT',   'i#l-3*<_-!_H6HX$M_J0+X[};`_2(UEY*IU7jRoq2B%&r^jmfD_^j<m,a(k_absM');
define('NONCE_SALT',       '!!8,/MYO/2JN+x>Qg+8>~DqjnSB^ytUUg}-M-t!|X?=VXf?V=-O[tT%s$;%SOSXB');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
